# environ-test

This is a test for building django applications using the gitlab-cli API, the problem is, that a lot of the tutorials out there stop at certain point and don't cover production details like, how do you pass env variables to the docker container so you don't have to track your .env file on the repo?

## Django Environ

Normaly my django applications use [django-environ](https://github.com/joke2k/django-environ) to read env variables from a `.env` file the problem is, when you build your project using gitlab, you don't have that file. My first aproach was to read the variables from the gitlab Secret variables on the settings page of the repo. The variables are passed to the `gilab-runner` that builds the docker container but not the container itself.

Searching everywhere I found the awesome [wemake-django-template](https://github.com/wemake-services/wemake-django-template) and [https://github.com/pydanny/cookiecutter-django/](https://github.com/pydanny/cookiecutter-django/) that use all the tools I wanted to use.

The solution is a small python package called [dump-env](https://github.com/sobolevn/dump-env) that reads OS variables and outputs them on a  `.env` file then you can pass arround that file to your docker containers and use `django-environ` as you would normally do.


## Notes

- Remember to use the prefix on the dot-env config on your gitlab variables
- You need to run a gitlab-runner with a docker in docker executor, that doesn't run on Mac, or I couldn't find a way of doing it.
- Dev Ops is dumb.
