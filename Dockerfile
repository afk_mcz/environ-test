FROM python:3.7-alpine
LABEL maintainer="afk@ellugar.co"

ENV PYTHONUNBUFFERED 1
RUN pip install pipenv
RUN mkdir /environ-test
WORKDIR /environ-test
ADD . /environ-test/
RUN ls -la /environ-test/
RUN echo "TEST OS==${TEST}"
RUN pipenv install --system --deploy --ignore-pipfile

RUN python index.py
